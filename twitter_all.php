<?php
namespace twitter;
require_once 'db.php';
?>
<meta charset="utf-8">
<style>
    div { border: thin solid black; }
</style>
<?php

foreach (\db\get_first_tweets() as $tweet)
{
    $tweet = (object)$tweet;
    $conv = [$tweet];
    ?><div><?php
    foreach (\db\get_tweet_replies($tweet->id) as $tweet)
        $conv[] = $tweet;
    foreach ($conv as $tw)
    {
        ?>
        <a href="http://twitter.com/_/status/<?=$tw->id?>"><?=$tw->user?></a>
        <p><?=$tw->text?></p>
        <?php
    }
    ?></div><?php
}
