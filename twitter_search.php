<?php
namespace twitter;
require_once 'db.php';
?>
<meta charset="utf-8">
<style>
    div { border: thin solid black; }
</style>
<?php

$q = $_GET['q'];

if (is_null($q))
{
    die (" like <a href='?q=Kelsier'>Kelsier</a>");
}

foreach (\db\search_tweets($q) as $tweet)
{
    $tweet = \db\find_first_tweet($tweet->id);
    $conv = [$tweet];
    ?><div><?php
    foreach (\db\get_tweet_replies($tweet->id) as $tweet)
        $conv[] = $tweet;
    foreach ($conv as $tw)
    {
        ?>
        <a href="http://twitter.com/_/status/<?=$tw->id?>"><?=$tw->user?></a>
        <p><?=$tw->text?></p>
        <?php
    }
    ?></div><?php
}
