<?php
namespace reddit;

function reddit_url($board, $thread, $post = null)
{
    $url = "https://www.reddit.com/r/{$board}/comments/{$thread}";
    if ($post != null)
        $url .= "/-/{$post}";
    return $url;
}

function self_url($board, $thread, $post = null)
{
    return '?'.$board.'/'.$thread.(is_null($post)?'':'/'.$post);
}

$param = explode('/',  $_SERVER['QUERY_STRING']);
if (count($param) < 2)
    die("eg <a href='?Mistborn/42o8aq/czneaap'>?Mistborn/42o8aq/czneaap</a>");

$url = reddit_url($param[0], $param[1], $param[2]).".json?context=10";

$stack = json_decode(file_get_contents($url));

while (count($stack))
{
    $self = array_shift($stack);
    if ($self->kind == 'Listing')
    {
        foreach($self->data->children as $obj)
            $stack[] = $obj;
        continue;
    }
    if ($self->kind == 't3')
    {
        $board = $self->data->subreddit;
        $thread = $self->data->id;
        $title = $self->data->title;
        $user = $self->data->author;
        $url = reddit_url($board, $thread);
        $_url = self_url($board, $thread);
        ?>
    <h1><?=$title?></h1>
    <a href="<?=$url?>"><?=$user?></a>
    <sup><small>[<a href="<?=$_url?>">self</a>]</small></sup>
    <p><?=html_entity_decode($self->data->selftext_html)?></p>
    <hr>
        <?php
        continue;
    }
    if ($self->kind == 't1')
    {
        #interpret replies next
        $stack[] = $self->data->replies;
        # print out
        $board = $self->data->subreddit;
        $thread = substr($self->data->link_id, 3);
        $user = $self->data->author;
        $url = reddit_url($board, $thread, $self->data->id);
        $_url = self_url($board, $thread, $self->data->id);
        ?>
    <a href="<?=$url?>"><?=$user?></a>
    <sup><small>[<a href="<?=$_url?>">self</a>]</small></sup>
    <p><?=html_entity_decode($self->data->body_html)?></p>
    <hr>
        <?php
        # don't do anything else
        continue;
    }
    var_dump($self);
}