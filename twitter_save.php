<?php
namespace twitter;
require_once 'lib/TwitterAPIExchange.php';
require_once 'db.php';
?>
<meta charset="utf-8">
<style>
    div { border: thin solid black; }
</style>
<?php

/** Set access tokens here - see: https://dev.twitter.com/apps/ **/
$settings = json_decode(file_get_contents("./config.json"), true)['twitter'];

$twitter = new \TwitterAPIExchange($settings);

function user_tweets($user, $b4=null)
{
    global $twitter;
    /** Perform a GET request and echo the response **/
    /** Note: Set the GET field BEFORE calling buildOauth(); **/
    $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    $getfield = '?screen_name='.$user.'&include_rts=false'.($b4?'&max_id='.$b4:'');
    $requestMethod = 'GET';
    $res = $twitter->setGetfield($getfield)
                 ->buildOauth($url, $requestMethod)
                 ->performRequest();
    return json_decode($res);
}

function tweet($id)
{
    global $twitter;
    /** Perform a GET request and echo the response **/
    /** Note: Set the GET field BEFORE calling buildOauth(); **/
    $url = 'https://api.twitter.com/1.1/statuses/show.json';
    $getfield = '?id='.$id;
    $requestMethod = 'GET';
    $res = $twitter->setGetfield($getfield)
                 ->buildOauth($url, $requestMethod)
                 ->performRequest();
    return json_decode($res);
}

$count = 0;

foreach (user_tweets('brandsanderson', $_GET['b4']) as $tweet)
{
    $conv = [$tweet];
    if (!\db\save_tweet($tweet))
        continue;
    $count += 1;
    while ($tweet->in_reply_to_status_id != null)
    {
        $tweet = tweet($tweet->in_reply_to_status_id);
        array_unshift($conv, $tweet);
        if (\db\save_tweet($tweet))
            $count += 1;
    }
}

print $count . ' tweets saved <br>';

?><a href="?b4=<?=end($conv)->id?>">later</a>
<script>
    setTimeout(function(){ window.location = document.querySelector('body>a').href; }, 100 * <?=$count?>);
</script>
