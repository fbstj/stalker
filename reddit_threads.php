<?php
namespace reddit;
require_once 'db.php';
?>
<meta charset="utf-8">
<style>
    div { border: thin solid black; }
</style>
<?php

function reddit_url($board, $thread, $post = null)
{
    $url = "https://www.reddit.com/r/{$board}/comments/{$thread}";
    if ($post != null)
        $url .= "/-/{$post}";
    return $url;
}

foreach (\db\get_reddit_threads() as $thread)
{
    ?><div><?php

    $url = reddit_url($thread->board, substr($thread->name, 3));
    ?>
    <h1><?=$thread->title?></h1>
    <a href="<?=$url?>"><?=$thread->user?></a>
    <p><?=html_entity_decode($thread->html)?></p>
    <?php
    
    foreach (\db\get_reddit_thread($thread->name) as $post)
    {
    $url = reddit_url($post->board, substr($post->thread, 3), substr($post->name, 3));
    ?>
    <a href="<?=$url?>?context=3"><?=$post->user?></a>
    <p><?=html_entity_decode($post->html)?></p>
        <?php
    }

    ?></div><?php
}
