<?php
namespace reddit;
require_once 'db.php';
?>
<meta charset="utf-8">
<style>
    div { border: thin solid black; }
</style>
<?php

function user_posts($user = 'mistborn', $after = null)
{
    $url = "https://www.reddit.com/user/{$user}.json";
    if ($after != null)
        $url .= '?after='.$after;
    return json_decode(file_get_contents($url));
}

function link_posts($sub, $link)
{
    $url = "https://www.reddit.com/r/{$sub}/comments/{$link}.json";
    return json_decode(file_get_contents($url))[1]->data->children[0];
}

$resp = user_posts('mistborn', $_GET['after']);

$count = 0;

foreach ($resp->data->children as $post)
{
    $kind = $post->kind;
    $post = $post->data;
    if (!\db\save_reddit($kind, $post))
        continue;
    $count += 1;
}

print $count . ' reddit posts saved <br>';

?><a href="?after=<?=$resp->data->after?>">next</a>
<script>
    setTimeout(function(){ window.location = document.querySelector('body>a').href; }, 100 * <?=$count?>);
</script>
