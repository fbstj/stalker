<?php
namespace db;
# library for database access
$DB = new \PDO("sqlite:./stalk.db");
$DB->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

$insert_tweet = $DB->prepare(<<<SQL
    INSERT OR IGNORE INTO
    Tweets (id,parent,text,user,json)
    VALUES ( ?,     ?,   ?,   ?,   ?)
SQL
);
function save_tweet($tweet)
{
    global $insert_tweet;
    $sucsess = $insert_tweet->execute([
        $tweet->id,
        $tweet->in_reply_to_status_id,
        $tweet->text,
        $tweet->user->screen_name,
        json_encode($tweet)
        ]);
    if (!$sucsess)
    {
        global $DB;
        var_dump($DB->errorInfo());
        var_dump($tweet);
        die();
    }
    return $insert_tweet->rowCount() != 0;
}

function get_first_tweets()
{
    global $DB;
    $q = $DB->prepare("SELECT * FROM Tweets WHERE parent IS NULL ORDER BY id DESC");
    $q->execute();
    return $q->fetchAll();
}

function search_tweets($query)
{
    global $DB;
    $q = $DB->prepare("SELECT * FROM Tweets WHERE text LIKE ? ORDER BY id DESC");
    $q->execute(["%{$query}%"]);
    return $q->fetchAll();
}

function get_tweet_replies($id)
{
    global $DB;
    $q = $DB->prepare("SELECT * FROM Tweets WHERE parent = ? ORDER BY id DESC");
    $ids = [$id];
    while (count($ids)) {
        $q->execute([array_shift($ids)]);
        foreach ($q->fetchAll() as $row)
        {
            yield $row;
            $ids[] = $row->id;
        }
    }
}

function find_first_tweet($id)
{
global $DB;
    $q = $DB->prepare("SELECT * FROM Tweets WHERE id = ?");
    while (!is_null($id))
    {
        $q->execute([$id]);
        $tweet = $q->fetch();
        $id = $tweet->parent;
    }
    return $tweet;
}

$insert_reddit = $DB->prepare(<<<SQL
    INSERT OR IGNORE INTO
    Reddits (name,kind,board,thread,parent,user,html,title,json)
    VALUES  (   ?,   ?,    ?,     ?,     ?,   ?,   ?,    ?,   ?)
SQL
);
function save_reddit($kind, $post)
{
    global $insert_reddit;
    if ($kind == 't3')
    {
        $html = $post->selftext_html;
        $title = $post->title;
    }
    else if ($kind == 't1')
    {
        $html = $post->body_html;
        $title = null;
    }
    $sucsess = $insert_reddit->execute([
        $post->name,
        $kind,
        $post->subreddit,
        $post->link_id,
        $post->parent_id,
        $post->author,
        $html,
        $title,
        json_encode($post)
        ]);
    if (!$sucsess)
    {
        global $DB;
        var_dump($DB->errorInfo());
        var_dump($post);
        die();
    }
    return $insert_reddit->rowCount() != 0;
}

function get_reddit_threads()
{
    global $DB;
    $q = $DB->prepare("SELECT * FROM Reddits WHERE kind = 't3'");
    $q->execute();
    return $q->fetchAll();
}

function get_reddit_thread($thread)
{
    global $DB;
    $q = $DB->prepare("SELECT * FROM Reddits WHERE thread = ?");
    $q->execute([$thread]);
    return $q->fetchAll();
}

function search_reddit($query)
{
    global $DB;
    $q = $DB->prepare("SELECT * FROM Reddits WHERE html LIKE ?");
    $q->execute(["%{$query}%"]);
    return $q->fetchAll();
}
